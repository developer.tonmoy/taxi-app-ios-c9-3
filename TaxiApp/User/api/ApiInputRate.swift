import UIKit
import Alamofire

class ApiInputRate {

    func doWork(driverid:String,rate:Int,manner:Int,clean:Int,driverstander:Int,comment:String?,done:@escaping()->()) {
        let url = "\(Url_.baseurl)api/driverRating"
        let headers = ["Content-Type": "application/json"]
        let param:Parameters = [
            "cleanliness": "\(clean)",
            "comment": "\(comment ?? "")",
            "driverId": "\(driverid)",
            "drivingStandard": "\(driverstander)",
            "key": "df2e54de-74c6-44a4-8941-785cb68d6d72",
            "manner": "\(manner)",
            "rating": "\(rate)",
            
        ]
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseData { data in
                done()
        }
        
    }


}
