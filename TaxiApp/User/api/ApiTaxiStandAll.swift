

import UIKit
import Alamofire

class ApiTaxiStandAll {

    
    func getAll(onComplete:@escaping(([TaxiStand])->())){
        let url = "\(Url_.baseurl)api/locations?key=df2e54de-74c6-44a4-8941-785cb68d6d72"
        
        Alamofire.request(url,method: .get)
            .validate().responseJSON { res in
                res.result.ifSuccess {
                    if let data = res.data{
                        do{
                          let taxistands =  try JSONDecoder().decode([TaxiStand].self, from: data)
                            onComplete(taxistands)
                        }catch{}
                    }
                    
                }
        }
    }
  

}
