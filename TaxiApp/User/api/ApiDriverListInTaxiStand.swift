
import UIKit
import Alamofire

class ApiDriverListInTaxiStand {
    let taxiStandID:String
    init(taxiStandID:String) {
        self.taxiStandID = taxiStandID
    }
    func doWork(onComplete:@escaping(([Driver])->()),noDriver:@escaping(()->())) {
        Alamofire.request(getUrl(),method: .get)
            .responseJSON { res in
                res.result.ifSuccess {
                    if let data = res.data{
                        
                        do{
                            let driverList = try JSONDecoder().decode([Driver].self, from: data)
                            onComplete(driverList)
                            if driverList.count<0{
                                noDriver()
                            }
                        }catch(let error){
                            print(error.localizedDescription)
                            noDriver()
                        }
                        
                    }
                    res.result.ifFailure {
                         noDriver()
                    }
                }
        }
    }
    private  func getUrl() -> String {
        let url = "\(Url_.baseurl)api/drivers?key=df2e54de-74c6-44a4-8941-785cb68d6d72&taxiStandId=\(taxiStandID)"
        return url
    }
}
