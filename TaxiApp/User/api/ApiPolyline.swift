
import UIKit
import CoreLocation
import Alamofire

class ApiPolyline {
    func doWork(orgin:CLLocationCoordinate2D, destination:CLLocationCoordinate2D,onComplete:@escaping(String,Routes)->())  {
        let url = getUrl(orgin: orgin,destination: destination)
        print("-------call")
        print(url)
        Alamofire.request(url,method: .get)
            .validate().responseJSON { res in
                
                
                if res.result.isSuccess {
                    if let data = res.data{
                        do{
                            let polyinfo = try JSONDecoder().decode(Polyinfo.self, from: data)
                            if polyinfo.routes != nil && polyinfo.routes?.count ?? 0>0{
                                if let route =  polyinfo.routes?[0]  {
                                   
                                    if let oa = route.overview_polyline?.points{
                                      onComplete(oa,route)
                                    }
                                }
                            }
                            
                            
                        }catch(let error ){
                            print(error)
                        }
                    }
                }
        }
        
    }
    private func getUrl(orgin:CLLocationCoordinate2D, destination:CLLocationCoordinate2D) ->String{
        return "https://maps.googleapis.com/maps/api/directions/json?origin=\(orgin.latitude),\(orgin.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&units=metric&mode=walking&key=\(Url_.googleApikey)"
    }
    
}
