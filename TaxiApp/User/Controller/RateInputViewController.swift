import UIKit
import Cosmos

class RateInputViewController: UIViewController {

    var driverid:Int = -1
    @IBOutlet weak var cleanliness: CosmosView!
    @IBOutlet weak var comment: UITextField!
    @IBOutlet weak var driverstander: CosmosView!
    @IBOutlet weak var manner: CosmosView!
    @IBOutlet weak var rate: CosmosView!
    @IBOutlet weak var lClean: UILabel!
    @IBOutlet weak var lDrivingStand: UILabel!
    @IBOutlet weak var lManner: UILabel!
    @IBOutlet weak var lTotalRate: UILabel!
    @IBOutlet weak var lSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setLanguage()
        
       
    }
    func setLanguage()  {
        self.lClean.text = Language.getString("Cleanliness")
        self.lDrivingStand.text = Language.getString("DrivingStandard")
        self.lManner.text = Language.getString("Manner")
        self.lTotalRate.text = Language.getString("TotalRating")
        self.lSubmit.setTitle(Language.getString("submit"), for: .normal)
        self.comment.placeholder = Language.getString("Comment")
    }
  

    
    @IBAction func submit(_ sender: UIButton) {
        
        let rate_ = Int(exactly: rate.rating) ?? 0
        let cleanliness_ = Int(exactly: cleanliness.rating) ?? 0
        let manner_ = Int(exactly: manner.rating) ?? 0
        let driverstander_ = Int(exactly: driverstander.rating) ?? 0
        let comment_ = comment.text
        
        ApiInputRate().doWork(driverid: "\(driverid)" , rate: rate_, manner: manner_, clean: cleanliness_, driverstander: driverstander_, comment: comment_) {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    

}
