import UIKit
import GooglePlaces
import CoreLocation
import GoogleMaps

class FareCalculateViewController: UIViewController,GMSAutocompleteResultsViewControllerDelegate,GMSMapViewDelegate {
    @IBOutlet weak var viewmap: UIView!
    
    var searchController: UISearchController?
    var resultsViewController: GMSAutocompleteResultsViewController?
    var mapView:GMSMapView? = nil
    var taxiStand:TaxiStand?
    var selectTaxiStand:CLLocationCoordinate2D?
    var polyline:GMSPolyline?
    var disMarker:GMSMarker? = nil
    var showfare:UIButton? = nil
    var dis:Int? = 0
    var placeName:String = "Fare Calculaton"
    override func viewDidLoad() {
        super.viewDidLoad()
        polyline = GMSPolyline()
        disMarker  = GMSMarker()
        polyline?.strokeWidth = 5
        polyline?.strokeColor = .red
        googleAutoCompleteSearch()
        setStandMarker()
        makeButton()
        self.showfare?.isHidden = true
       
    }
     func makeButton() {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        showfare = UIButton(frame: CGRect(x: 0, y: screenHeight-50, width: screenWidth, height: 50))
        showfare?.backgroundColor = .systemYellow
        showfare?.setTitle("SHOW FARE", for: .normal)
        showfare?.setTitleColor(.black, for: .normal)
        showfare?.addTarget(self, action: #selector(showFareButtonClick), for: .touchUpInside)
        self.view.addSubview(showfare!)
     }
    @objc func showFareButtonClick(_ sender: UIButton) {
       
        self.showFareCalcu(dis ?? 0, placeName: placeName)
        
    }
    override func loadView() {
        let gframe = CGRect(x: 0, y: 0, width: 1, height: 1)
        let camera = GMSCameraPosition.camera(withLatitude: 48.208133, longitude:16.373937, zoom: 6.0)
        mapView = GMSMapView.map(withFrame: gframe, camera: camera)
        mapView?.delegate = self
       //mapView?.isMyLocationEnabled = true
        //mapView?.settings.myLocationButton = true
        self.view = mapView
    }
    func setStandMarker() {
        
        if let st = taxiStand{
        
            let lat = CLLocationDegrees(st.lat!)
            let lng = CLLocationDegrees(st.lng!)
            self.selectTaxiStand = CLLocationCoordinate2D(latitude: lat ?? 0.0, longitude: lng ?? 0.0)
            mapView?.camera = GMSCameraPosition(target: selectTaxiStand!, zoom: 16.0)
            let marker = GMSMarker(position:selectTaxiStand!)
            marker.map = self.mapView
        }
        
    }
    func googleAutoCompleteSearch(){
        resultsViewController = GMSAutocompleteResultsViewController()
               let filter = GMSAutocompleteFilter()
              filter.country = "BD"
              filter.type = .noFilter
              let orgin  = CLLocation(latitude: 48.209059, longitude: 16.375783)
              filter.origin = orgin
              resultsViewController?.autocompleteFilter = filter
              resultsViewController?.delegate = self
              searchController = UISearchController(searchResultsController: resultsViewController)
              searchController?.searchResultsUpdater = resultsViewController
               searchController?.searchBar.sizeToFit()
               searchController?.searchBar.placeholder = "Place Search"
              navigationItem.titleView = searchController?.searchBar
//              searchController?.searchBar.frame = (CGRect(x: 0, y: 0, width: 250.0, height: 44.0))
//              navigationItem.rightBarButtonItem = UIBarButtonItem(customView: (searchController?.searchBar)!)

              // When UISearchController presents the results view, present it in
              // this view controller, not one further up the chain.
              definesPresentationContext = true
              // Prevent the navigation bar from being hidden when searching.
              searchController?.hidesNavigationBarDuringPresentation = false
              //searchController?.modalPresentationStyle = .popover
    }
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
           searchController?.isActive = false
        self.polyline?.map = nil
        if let st = selectTaxiStand{
            
            ApiPolyline().doWork(orgin: st, destination: place.coordinate) { encodepath , route in
                
                let path = GMSPath(fromEncodedPath: encodepath)
                
                self.mapView?.animate(to: GMSCameraPosition(target: place.coordinate, zoom: 15))
                self.disMarker?.map = nil
                self.disMarker?.position = place.coordinate
                self.disMarker?.icon = GMSMarker.markerImage(with: .green)
                self.disMarker?.map = self.mapView
                self.polyline?.path = path
                self.polyline?.map = self.mapView
                
                if let distance =  route.legs?.first?.distance?.value{
                    self.dis = distance
                    self.placeName = place.name ?? "Fare Calculaton"
                    self.showfare?.isHidden = false
                }
                
                
                
                
            }
        }
          
           //dismiss(animated: true, completion: nil)
           
    }
    func showFareCalcu(_ distance: Int, placeName:String) {
       let price =  FareCalculator().calculate(distance: Double(distance))
        let priceStr = String(format: "%.2f",price)
        let dis:Double = Double(distance)/1000.0
        let distanceStr = String(format: "%.2f", dis)
        
        let message = "Distance: \(distanceStr) KM\nCash: \(priceStr) €"
        
        
        let alert = UIAlertController(title: placeName, message: message , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert,animated: true)
        
    }
       
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
            print("Error: ", error.localizedDescription)
    }
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
       UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
       UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
   

    
    

  
}


