import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

class UserHomeViewController: UIViewController,CLLocationManagerDelegate ,GMSMapViewDelegate {
    var locationManager:CLLocationManager? = nil
    var placesClient:GMSPlacesClient? = nil
    var mapView:GMSMapView? = nil
    var polyline = GMSPolyline()
    var delegate: HomeControllerDelegate?
    var loginClick:(()->())? = nil
    var orginUpdate:CLLocationCoordinate2D? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
        locationManager?.distanceFilter =  50
        locationManager?.startUpdatingLocation()
        locationManager?.delegate = self
        
        placesClient = GMSPlacesClient.shared()
        markTaxiStand()
        //self.navigationController?.pushViewController(ContainerController(), animated: true)
      
         configureNavigationBar()
        
       
        
        loginClick = {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let lc = LocalSaved().getDriverID()
            
            if !lc.isEmpty && lc != "-1" {
                
              let vc = DriverContainerController()
              self.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                
                let driverlistVC = storyboard.instantiateViewController(withIdentifier: "driverloginco") as! DriverLoginViewController
                self.navigationController?.pushViewController(driverlistVC, animated: true)
            }
            
                       
        }
        
        testlanguage()
        
    }
    func testlanguage()  {
        
       // Language.setCurrentLanguage("de")
        print("---------------------------")
        print(Language.getString("bear"))
    }
    @objc func handleMenuToggle() {
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
    
    func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = .systemYellow
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.black
        
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_white_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenuToggle))
   
        navigationItem.title = "Taxi App"
      
    
    
    }
    func markTaxiStand(){
        ApiTaxiStandAll().getAll { stands in
                  
                   for st in stands{
                       if st.lat != nil && st.lng != nil{
                       let lat = CLLocationDegrees(st.lat!)
                       let lng = CLLocationDegrees(st.lng!)
                       let marker = GMSMarker()
                        
                       let latlon = CLLocationCoordinate2D(latitude:lat ?? 0.0, longitude: lng ?? 0.0)
                       marker.position = latlon
                       marker.title = st.district
                       marker.snippet = st.address
                       marker.userData = st
                       marker.icon = UIImage(named:"standmarker")
                       marker.map = self.mapView
                       
                        
                       }
                   }
               }
        
    }
    override func loadView() {
              let gframe = CGRect(x: 0, y: 0, width: 1, height: 1)
              let camera = GMSCameraPosition.camera(withLatitude: 48.208133, longitude:16.373937, zoom: 6.0)
               mapView = GMSMapView.map(withFrame: gframe, camera: camera)
               mapView?.delegate = self
               mapView?.isMyLocationEnabled = true
               mapView?.settings.myLocationButton = true
               self.view = mapView
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
           let location: CLLocation = locations.last!
           self.orginUpdate = location.coordinate
           googleMapUpdate(location: location)
           
       }
    
    func googleMapUpdate(location: CLLocation) {
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                     longitude: location.coordinate.longitude,zoom: 16)
       mapView?.animate(to: camera)
        
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
              UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
              placesClient?.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: fields, callback: { placeLikelihoodList, error in
                  
                  if let error = error {
                    print("An error occurred: \(error.localizedDescription)")
                    return
                  }

                  if placeLikelihoodList != nil {
                      self.navigationItem.title = placeLikelihoodList?.last?.place.name
                  }
                  
              })
       
          
               
    }
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let ts = marker.userData as! TaxiStand
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let driverlistVC = storyboard.instantiateViewController(withIdentifier: "driverlist") as! DriverListViewController
        driverlistVC.driverid = String(ts.id ?? 0)
        driverlistVC.taxiStand = ts
        self.navigationController?.pushViewController(driverlistVC, animated: true)
        
        
        
        
        
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.polyline.map = nil
        
        if orginUpdate == nil{
        
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
        
        placesClient?.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: fields, callback: { placeLikelihoodList , error  in
            
            if let error = error {
              print("An error occurred: \(error.localizedDescription)")
              return
            }

            if let placeLikelihoodList_ = placeLikelihoodList {
                if let orgin = placeLikelihoodList_.last?.place.coordinate{
                    self.drawPolyline(orgin: orgin, destination:  marker.position)
                }
            }
        })
        }else{
            drawPolyline(orgin: orginUpdate!, destination: marker.position)
        }
        return false
    }
    func drawPolyline(orgin:CLLocationCoordinate2D, destination:CLLocationCoordinate2D){
        ApiPolyline().doWork(orgin:orgin, destination:destination ,onComplete: { points , route in
                           
                           let rect = GMSPath(fromEncodedPath:points )
                           self.polyline.path = rect
                           self.polyline.strokeColor = .blue
                           self.polyline.strokeWidth = 5
                           self.polyline.map = self.mapView
                           
                       })
    }


}

