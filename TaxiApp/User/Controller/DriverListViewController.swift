import UIKit

class DriverListViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    var driverlist:[Driver] = []
    var driverid:String = ""
    var taxiStand:TaxiStand?
    var isdriver:Bool = false
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var farecalculatebutton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.lightGray
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        farecalculatebutton.setTitle(Language.getString("farecalculate"), for: .normal)
        
        
        ApiDriverListInTaxiStand(taxiStandID: driverid)
            .doWork (onComplete:{ driverlist in
                self.driverlist = driverlist
                self.tableView.reloadData()}, noDriver :{
                    print("no driver ")
                    let alert = UIAlertController(title: "Info", message: "No Driver found ", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true)
            }
        )
        

      
    }
    @IBAction func fareCalculateAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "farecalcuate") as! FareCalculateViewController
        vc.taxiStand = self.taxiStand
        self.navigationController?.pushViewController(vc, animated: true)
      }
      

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return driverlist.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "drivercell") as! DriverTableViewCell
        let driver = driverlist[indexPath.row]
        let rat = Float(driver.avgRating ?? "0.0")
        cell.selectionStyle = .none
        cell.firstname.text = driver.firstName
        cell.lastName.text = driver.username
        cell.number.text = driver.numberPlate
        cell.rating.text = String(format: "%.2f", rat ?? 0.0)
        cell.detailsButton.tag = indexPath.row
        cell.detailsButton.addTarget(self, action: #selector(pressed), for: .touchUpInside)
        return cell
           
       }
    @objc func pressed(sender: UIButton!) {
        let p = sender.tag
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "driver_details") as! DriverDetailsViewController
        let driver = driverlist[p]
        vc.driver = driver
        vc.isdriver = self.isdriver
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    

   

}
