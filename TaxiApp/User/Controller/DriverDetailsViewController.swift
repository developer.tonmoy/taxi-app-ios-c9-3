import UIKit

class DriverDetailsViewController: UIViewController {

    var isdriver:Bool = false
    
    @IBOutlet weak var firstname: UILabel!
    @IBOutlet weak var lastname: UILabel!
    @IBOutlet weak var curnumber: UILabel!
    @IBOutlet weak var payment: UILabel!
    @IBOutlet weak var eauto: UILabel!
    @IBOutlet weak var animal: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var phonenumber: UILabel!
    
    @IBOutlet weak var startride: UIButton!
  
    @IBOutlet weak var endride: UIButton!
    
    @IBOutlet weak var closeride: UIButton!
    @IBOutlet weak var usernamse: UILabel!
    
    @IBOutlet weak var buttonHolderview: UIView!
    
    //for language change
    @IBOutlet weak var personaldetais: UILabel!
    @IBOutlet weak var lFirestname: UILabel!
    
    @IBOutlet weak var lLastname: UILabel!
    
    @IBOutlet weak var lVechlenumber: UILabel!
    @IBOutlet weak var lPhoneNumber: UILabel!
    
    @IBOutlet weak var lCreditcard: UILabel!
    @IBOutlet weak var lAnimal: UILabel!
    @IBOutlet weak var lEAuto: UILabel!
    
    var driver:Driver? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        displayData()
        buttonAction()
        
        setLanguge()
       
        
    }
    private func setLanguge(){
    
        self.personaldetais.text = Language.getString("personaldetails")
        self.lFirestname.text = Language.getString("")
        self.lVechlenumber.text  = Language.getString("Viechle")
        
         self.lPhoneNumber.text  = Language.getString("Phone")
         self.lCreditcard.text  = Language.getString("CreditCardPayment")
         self.lAnimal.text  = Language.getString("Animal")
         self.lEAuto.text  = Language.getString("Auto")
         self.lVechlenumber.text  = Language.getString("Viechle")
         self.startride.setTitle(Language.getString("startride"), for: .normal)
         self.closeride.setTitle(Language.getString("closeride"), for: .normal)
         self.endride.setTitle(Language.getString("endride"), for: .normal)
    }
    private func buttonAction(){
        if isdriver {
            startride.isHidden = true
            buttonHolderview.isHidden = true
        }
        startride.addTarget(self, action: #selector(startRideClick), for: .touchUpInside)
        endride.addTarget(self, action: #selector(endrideAndCloseRide), for: .touchUpInside)
        closeride.addTarget(self, action: #selector(endrideAndCloseRide), for: .touchUpInside)
    }
    @objc func startRideClick(){
        startride.isHidden = true
    }
    @objc func endrideAndCloseRide(){
        startride.isHidden = false
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "rateinput") as! RateInputViewController
        vc.driverid = driver?.id ?? -1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func displayData(){
        if driver != nil {
                   firstname.text = driver?.firstName ?? ""
                   lastname.text = driver?.lastName ?? ""
                    usernamse.text =  driver?.username ?? ""
                   curnumber.text = driver?.numberPlate ?? ""
                   phonenumber.text = driver?.mobileNumber ?? "-"
                   if driver?.creditCardPayment ?? false{
                       payment.text =  "true"
                   }else{
                        payment.text =  "false"
                   }
                   
                   if driver?.animal ?? false{
                      animal.text =  "true"
                  }else{
                      animal.text =  "false"
                   }
                   
            self.image.image = UIImage(named: "profile")
            CommonUtils.loadImage(url:"\(Url_.imageurl)\(driver?.profileImageUrl ?? "" )") { img in
                       self.image.image = img
                   }
               }
    }
    


}
