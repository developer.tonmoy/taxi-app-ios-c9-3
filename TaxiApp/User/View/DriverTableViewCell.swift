//
//  DriverTableViewCell.swift
//  TaxiApp
//
//  Created by 4axizit on 3/3/20.
//  Copyright © 2020 4axiz. All rights reserved.
//

import UIKit

class DriverTableViewCell: UITableViewCell {

    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var blackview: UIView!
    let cornerRadius:CGFloat = 10.0
    @IBOutlet weak var firstname: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var rating: UILabel!
    
    @IBOutlet weak var fahrerdetailsbutton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.detailsButton.layer.cornerRadius = cornerRadius
        self.numberView.layer.cornerRadius = cornerRadius
        self.blackview.layer.cornerRadius = cornerRadius
        self.fahrerdetailsbutton.setTitle(Language.getString("fahrerdetais"), for: .normal)
        
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
