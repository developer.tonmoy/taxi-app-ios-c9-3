import Foundation
struct Driver : Codable {
	let id : Int?
	let animal : Bool?
	let avgRating : String?
	let createdBy : CreatedBy?
	let creditCardPayment : Bool?
	let dateOfBirth : String?
	let deviceId : String?
	let driverId : String?
	let electricCar : Bool?
	let firstName : String?
	let lastName : String?
	let mobileNumber : String?
	let numberPlate : String?
	let online : Bool?
	let password : String?
	let profileImageUrl : String?
	let ratingCount : String?
	let username : String?
	let vehicleType : String?
	let workFor : WorkFor?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case animal = "animal"
		case avgRating = "avgRating"
		case createdBy = "createdBy"
		case creditCardPayment = "creditCardPayment"
		case dateOfBirth = "dateOfBirth"
		case deviceId = "deviceId"
		case driverId = "driverId"
		case electricCar = "electricCar"
		case firstName = "firstName"
		case lastName = "lastName"
		case mobileNumber = "mobileNumber"
		case numberPlate = "numberPlate"
		case online = "online"
		case password = "password"
		case profileImageUrl = "profileImageUrl"
		case ratingCount = "ratingCount"
		case username = "username"
		case vehicleType = "vehicleType"
		case workFor = "workFor"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		animal = try values.decodeIfPresent(Bool.self, forKey: .animal)
		avgRating = try values.decodeIfPresent(String.self, forKey: .avgRating)
		createdBy = try values.decodeIfPresent(CreatedBy.self, forKey: .createdBy)
		creditCardPayment = try values.decodeIfPresent(Bool.self, forKey: .creditCardPayment)
		dateOfBirth = try values.decodeIfPresent(String.self, forKey: .dateOfBirth)
		deviceId = try values.decodeIfPresent(String.self, forKey: .deviceId)
		driverId = try values.decodeIfPresent(String.self, forKey: .driverId)
		electricCar = try values.decodeIfPresent(Bool.self, forKey: .electricCar)
		firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
		lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
		mobileNumber = try values.decodeIfPresent(String.self, forKey: .mobileNumber)
		numberPlate = try values.decodeIfPresent(String.self, forKey: .numberPlate)
		online = try values.decodeIfPresent(Bool.self, forKey: .online)
		password = try values.decodeIfPresent(String.self, forKey: .password)
		profileImageUrl = try values.decodeIfPresent(String.self, forKey: .profileImageUrl)
		ratingCount = try values.decodeIfPresent(String.self, forKey: .ratingCount)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		vehicleType = try values.decodeIfPresent(String.self, forKey: .vehicleType)
		workFor = try values.decodeIfPresent(WorkFor.self, forKey: .workFor)
	}

}
