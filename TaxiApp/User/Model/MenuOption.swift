//
//  MenuOption.swift
//  SideMenuTutorial
//
//  Created by Stephen Dowless on 12/12/18.
//  Copyright © 2018 Stephan Dowless. All rights reserved.
//

import UIKit


enum MenuOption: Int, CustomStringConvertible {
    
    case Home
    case LoginDriver
    case Impressum
    case Language_

    var description: String {
        switch self {
        case .Home: return Language.getString("Home")
        case .LoginDriver: return Language.getString("LoginDriver")
        case .Impressum : return Language.getString("Impressum")
        case .Language_: return Language.getString("Language")
        }
    }
    
    var image: UIImage {
        switch self {
        case .Home: return UIImage(named: "home") ?? UIImage()
        case .LoginDriver: return UIImage(named: "login") ?? UIImage()
        case .Impressum: return UIImage(named: "info") ?? UIImage()
        case .Language_: return UIImage(named: "global") ?? UIImage()
        }
    }
}
