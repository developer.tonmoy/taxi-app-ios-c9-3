
import Foundation
struct TaxiStand : Codable {
	let id : Int?
	let address : String?
	let district : String?
	let internalIdentificationNumber : String?
	let internalNumber : String?
	let lat : String?
	let lng : String?
	let phoneStand : String?
	let taxiLimit : String?
	let timeRestriction : String?
	let visible : Bool?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case address = "address"
		case district = "district"
		case internalIdentificationNumber = "internalIdentificationNumber"
		case internalNumber = "internalNumber"
		case lat = "lat"
		case lng = "lng"
		case phoneStand = "phoneStand"
		case taxiLimit = "taxiLimit"
		case timeRestriction = "timeRestriction"
		case visible = "visible"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		district = try values.decodeIfPresent(String.self, forKey: .district)
		internalIdentificationNumber = try values.decodeIfPresent(String.self, forKey: .internalIdentificationNumber)
		internalNumber = try values.decodeIfPresent(String.self, forKey: .internalNumber)
		lat = try values.decodeIfPresent(String.self, forKey: .lat)
		lng = try values.decodeIfPresent(String.self, forKey: .lng)
		phoneStand = try values.decodeIfPresent(String.self, forKey: .phoneStand)
		taxiLimit = try values.decodeIfPresent(String.self, forKey: .taxiLimit)
		timeRestriction = try values.decodeIfPresent(String.self, forKey: .timeRestriction)
		visible = try values.decodeIfPresent(Bool.self, forKey: .visible)
	}

}
