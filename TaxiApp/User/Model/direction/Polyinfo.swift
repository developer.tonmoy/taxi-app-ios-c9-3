
import Foundation
struct Polyinfo : Codable {
	let geocoded_waypoints : [Geocoded_waypoints]?
	let routes : [Routes]?
	let status : String?

	enum CodingKeys: String, CodingKey {

		case geocoded_waypoints = "geocoded_waypoints"
		case routes = "routes"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		geocoded_waypoints = try values.decodeIfPresent([Geocoded_waypoints].self, forKey: .geocoded_waypoints)
		routes = try values.decodeIfPresent([Routes].self, forKey: .routes)
		status = try values.decodeIfPresent(String.self, forKey: .status)
	}

}
