import Foundation
struct Steps : Codable {
	let distance : Distance?
	let duration : Duration?
	let end_location : End_location?
	let html_instructions : String?
	let polyline : Polyline?
	let start_location : Start_location?
	let travel_mode : String?

	enum CodingKeys: String, CodingKey {

		case distance = "distance"
		case duration = "duration"
		case end_location = "end_location"
		case html_instructions = "html_instructions"
		case polyline = "polyline"
		case start_location = "start_location"
		case travel_mode = "travel_mode"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		distance = try values.decodeIfPresent(Distance.self, forKey: .distance)
		duration = try values.decodeIfPresent(Duration.self, forKey: .duration)
		end_location = try values.decodeIfPresent(End_location.self, forKey: .end_location)
		html_instructions = try values.decodeIfPresent(String.self, forKey: .html_instructions)
		polyline = try values.decodeIfPresent(Polyline.self, forKey: .polyline)
		start_location = try values.decodeIfPresent(Start_location.self, forKey: .start_location)
		travel_mode = try values.decodeIfPresent(String.self, forKey: .travel_mode)
	}

}
