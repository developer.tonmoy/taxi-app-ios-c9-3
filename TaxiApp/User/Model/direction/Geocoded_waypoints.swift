
import Foundation
struct Geocoded_waypoints : Codable {
	let geocoder_status : String?
	let place_id : String?
	let types : [String]?

	enum CodingKeys: String, CodingKey {

		case geocoder_status = "geocoder_status"
		case place_id = "place_id"
		case types = "types"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		geocoder_status = try values.decodeIfPresent(String.self, forKey: .geocoder_status)
		place_id = try values.decodeIfPresent(String.self, forKey: .place_id)
		types = try values.decodeIfPresent([String].self, forKey: .types)
	}

}
