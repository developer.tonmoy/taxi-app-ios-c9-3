
import Foundation
struct Duration : Codable {
	let text : String?
	let value : Int?

	enum CodingKeys: String, CodingKey {

		case text = "text"
		case value = "value"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		value = try values.decodeIfPresent(Int.self, forKey: .value)
	}

}
