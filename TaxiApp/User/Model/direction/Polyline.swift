
import Foundation
struct Polyline : Codable {
	let points : String?

	enum CodingKeys: String, CodingKey {

		case points = "points"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		points = try values.decodeIfPresent(String.self, forKey: .points)
	}

}
