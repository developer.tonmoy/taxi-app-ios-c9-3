
import Foundation
struct OtherField : Codable {
	let categories : String?
	let functions : String?
	let furtherActions : String?
	let id : Int?
	let interests : String?
	let templateName : String?

	enum CodingKeys: String, CodingKey {

		case categories = "categories"
		case functions = "functions"
		case furtherActions = "furtherActions"
		case id = "id"
		case interests = "interests"
		case templateName = "templateName"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		categories = try values.decodeIfPresent(String.self, forKey: .categories)
		functions = try values.decodeIfPresent(String.self, forKey: .functions)
		furtherActions = try values.decodeIfPresent(String.self, forKey: .furtherActions)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		interests = try values.decodeIfPresent(String.self, forKey: .interests)
		templateName = try values.decodeIfPresent(String.self, forKey: .templateName)
	}

}
