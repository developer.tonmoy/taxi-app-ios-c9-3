
import UIKit
import Alamofire

class CommonUtils {

   public static func loadImage(url:String,onLoad:(@escaping(UIImage)->Void)){
        Alamofire.request(url)
                   .responseData { (res) in
                       if(res.result.isSuccess){
                          if let  data = res.data{
                            if let image = UIImage(data: data){
                                 onLoad(image)
                            }
                           
                              
                           }
                          else{
                            print("image null ")
                           }
                           
                       }
                       if(res.result.isFailure){
                           //print(res.result.error?.localizedDescription)
                       }
                       
                }
    }
  public static  func resizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage? {
        let size = image.size

        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
    

}
