import UIKit

class FareCalculator {
    
    func calculate(distance:Double)->Double {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let week = calendar.component(.weekday, from: date)
        var startRate:Double = 0.0
        let km:Double = distance/1000
        var centPerMetter:Double = 0.0
        var totalPrice = 0.0

        if (week>=2 && week<=6) && (hour>=6 && hour<=22)  {
            print(week)
            startRate = 3.80
            switch km {
            case 0.0...4.0:do {
                centPerMetter = 20.0/140.70
                break
            }
                
            case 4.0...9.0: do {
                centPerMetter = 20.0/184.60
                break
            }
                
            default: do {
                centPerMetter = 20.0/190.60
                break
            }
                
            }
            
            
        }else{
        
            startRate = 4.30
            switch km {
                   case 0.0...4.0:do {
                       centPerMetter = 20.0/123.20
                       break
                   }
                       
                   case 4.0...9.0: do {
                       centPerMetter = 20.0/156.80
                       break
                   }
                       
                   default: do {
                       centPerMetter = 20.0/169.50
                       break
                   }
                       
                   }
            
            
        }
        totalPrice = startRate + ((centPerMetter*distance)/100)
        
       
     return totalPrice
        
    }

   

}
