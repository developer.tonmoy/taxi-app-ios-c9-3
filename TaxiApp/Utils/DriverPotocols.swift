
protocol DriverHomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: DriverMenuOption?)
}
