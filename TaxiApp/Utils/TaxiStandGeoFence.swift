import UIKit
import CoreLocation
import GoogleMaps
import UserNotifications

class TaxiStandGeoFence {
    var listStand:[GMSMarker]
    var radius:Int = 0
    var postLocations:[GMSMarker] = []
    
    init(list:[GMSMarker],radius:Int){
        self.listStand = list
        self.radius = radius
    }
    func findPlace(currentLocation:CLLocation){
        var preLocatons:[GMSMarker] = []
        for st in  listStand {
            let position = st.position
            let standLocation = CLLocation(latitude: position.latitude, longitude: position.longitude)
            let distance = standLocation.distance(from: currentLocation)
            
            if Int(distance) <= radius {
                preLocatons.append(st)
            }
        }
        
        let exitlist =  postLocations.filter { marker -> Bool in
            !preLocatons.contains(marker)
        }
        let enterlist =  preLocatons.filter { marker -> Bool in
            !postLocations.contains(marker)
        }
        var enters:[String] = []
        var exits:[String] = []
        enterlist.forEach { marker in
            enters.append(marker.snippet ?? "No Name")
        }
        taxiStandMangement(entermarker: enterlist, currentloaction: currentLocation)
        exitlist.forEach { marker in
            exits.append(marker.snippet ?? "No Name")
        }
        
        
        if !enters.isEmpty{
           let message =  "\(enters.joined(separator: ",")) taxi stand"
            print(message)
            sendNotification(message, title: "You enter in")
        }
        if !exits.isEmpty{
           let message =  "\(exits.joined(separator: ",")) taxt stand"
            print(message)
            sendNotification(message, title: "You exit in")
        }
        
    
        self.postLocations = preLocatons
        
       
    }
    func taxiStandMangement(entermarker:[GMSMarker],currentloaction:CLLocation) {
        if !entermarker.isEmpty{
            let st =  entermarker.last?.userData as! TaxiStand
            setstand(taxiStandId:  String(st.id ?? -1))
            print("Save stand")
        }
        
    }
    private func setstand(taxiStandId:String){
        ApiDriverLocation().doWork(driverId: LocalSaved().getDriverID(), taxiStandId: taxiStandId)
    }
    private func sendNotification( _ message:String,title:String){
        if #available(iOS 10.0, *) {
                   let content = UNMutableNotificationContent()
                   content.title = title
                   content.body = message
                   content.sound = UNNotificationSound.default
                  // let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest(identifier: "taxistand \(title)", content: content, trigger:.none)
                   UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
               }
    }
    
}
