import CoreLocation
import GeoFire
import Firebase

class DriverToDriverGeoFire {
    public var currentlocation:CLLocation? = nil
    var geofire:GeoFire
    var postEnterList:[String] = []
    var drivername:String? = nil
    var timer:Timer? = nil
    //this is test comment
    init(drivername:String) {
        let ref = Database.database().reference()
        self.geofire = GeoFire(firebaseRef: ref)
        self.drivername = drivername
    }
    func sendNotification( _ message:String,title:String){
        if #available(iOS 10.0, *) {
                   let content = UNMutableNotificationContent()
                   content.title = title
                   content.body = message
                   content.sound = UNNotificationSound.default
                  // let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest(identifier: "taxidriver", content: content, trigger:.none)
                   UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
               }
    }
    
    
    
    func updateLocationData(currentlocation:CLLocation)  {
        self.currentlocation = currentlocation
        self.geofire.setLocation(currentlocation, forKey:  drivername!)
    }
    func start() {
        if #available(iOS 10.0, *) {
            if timer == nil{
                timer =  Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { tiemr in
                self.query()
            })
            }else{
                 print("Already start")
            }
           
        }
    }
    func stop() {
        if timer != nil{
            self.timer?.invalidate()
            self.timer = nil
        }else{
            print("Not start")
        }
    }
    private func query() {
        print("call")
        if currentlocation != nil {
             var currentEnterList:[String] = []
                 
             let queryHendler = geofire.query(at: currentlocation!, withRadius: 0.2)
             queryHendler.observe(.keyEntered, with: {
                     drivername , location in
                if self.drivername != drivername {
                     currentEnterList.append(drivername)
                }
                     
             })
             queryHendler.observeReady {
                 
                 print("current location :\(currentEnterList)")
                     
                 let realEnterlist =  currentEnterList.filter { cd -> Bool in
                     !self.postEnterList.contains(cd)
                 }
                print("post location :\(self.postEnterList)")
                     
                 if realEnterlist.count > 0 {
                         let driverNames = realEnterlist.joined(separator: ",")
                    print(self.drivername!)
                     self.sendNotification(driverNames, title: "Near to you ")
                         
                 }
                 self.postEnterList = currentEnterList
                 
             }
             
        }else{
            print("Current location is nil ")
        }
    }
   
}
