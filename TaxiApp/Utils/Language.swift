import UIKit

class Language {
    public static func getString(_ key:String) -> String {
        
        let applanguage = getCurrentLanguage()
            
        if applanguage == nil{
            setCurrentLanguage(Locale.current.languageCode ?? "en")
        }
        
        guard let bundlePath = Bundle.main.path(forResource: applanguage, ofType: "lproj"), let bundle = Bundle(path: bundlePath) else {return NSLocalizedString(key, comment: "")}
        
        return NSLocalizedString(key, tableName: nil, bundle: bundle, comment: "")
        
    }
    public static func setCurrentLanguage(_ lan:String){
        UserDefaults.standard.set(lan, forKey: "taxiapplanguage")
    }
    public static func getCurrentLanguage() -> String?{
        return  UserDefaults.standard.string(forKey: "taxiapplanguage")
    }
    

}
