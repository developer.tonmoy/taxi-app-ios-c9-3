import UIKit

class ImpressumViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        let text = UITextView()
        text.text = Language.getString("ImpressumText")
        text.font = UIFont(name: text.font!.fontName, size: 14)
        text.textColor = .black
        text.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(text)
        text.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 12).isActive = true
        text.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -12).isActive = true
        text.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        text.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
       
    }

       
       @objc func handleDismiss() {
           dismiss(animated: true, completion: nil)
       }
       
       // MARK: - Helper Functions
       
       func configureUI() {
        view.backgroundColor  = .white

           navigationController?.navigationBar.barTintColor = .darkGray
           if #available(iOS 11.0, *) {
               navigationController?.navigationBar.prefersLargeTitles = true
           } else {
               // Fallback on earlier versions
           }
           navigationItem.title = "Impressum"
           let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
           navigationController?.navigationBar.titleTextAttributes = textAttributes
          // navigationController?.navigationBar.barStyle = .black
           
           navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleDismiss))
        
        
       }
    

  

}
