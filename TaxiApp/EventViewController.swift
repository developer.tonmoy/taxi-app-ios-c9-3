

import UIKit
import Alamofire

class EventViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.hidesBackButton = true
        call_api()
        
    }
    func call_api(){
        let url = UrlList.baseUrl+"fla-events"
        let lo = LocalSaved()
       
        let headers = ["Authorization": "Bearer \(lo.getToken())"]
        
        Alamofire.request(url, method: .get, headers:headers).validate().responseJSON { res in
            if(res.result.isSuccess){
                guard let data = res.data else {return}
                do{
                let swObject = try JSONDecoder().decode([Event].self, from: data)
                print(swObject)
                }catch let error{
                    print(error)
                }
                
               
            }
            if(res.result.isFailure){
                print(res.response?.statusCode)
                
            }
           
        }
    }
   
    

}
