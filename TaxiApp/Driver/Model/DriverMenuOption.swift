import UIKit

enum DriverMenuOption: Int, CustomStringConvertible {
    
    case Review
    case Impressum
    case Logout
    case Language_
    
    var description: String {
        switch self {
        case .Review: return Language.getString("Review")
        case .Impressum: return Language.getString("Impressum")
        case .Logout: return Language.getString("Logout")
        case .Language_: return Language.getString("Language")
        }
    }
    
    var image: UIImage {
        switch self {
        case .Review: return UIImage(named: "review") ?? UIImage()
        case .Impressum: return UIImage(named: "info") ?? UIImage()
        case .Logout: return UIImage(named: "logout") ?? UIImage()
        case .Language_: return UIImage(named: "global") ?? UIImage()
        }
    }
}
