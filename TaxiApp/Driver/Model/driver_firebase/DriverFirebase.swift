import Foundation
import CoreLocation

struct DriverFirebase {
    var name:String
    var location:CLLocation
    init(name:String,location:CLLocation ) {
        self.name = name
        self.location = location
    }
}
