import Foundation
struct DriverRate : Codable {
	var id : Int?
	var cleanliness : Int?
	var comment : String?
	var dateCreated : String?
	var driver : Driver?
	var drivingStandard : Int?
	var enabled : Bool?
	var lastUpdated : String?
	var manner : Int?
	var rating : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case cleanliness = "cleanliness"
		case comment = "comment"
		case dateCreated = "dateCreated"
		case driver = "driver"
		case drivingStandard = "drivingStandard"
		case enabled = "enabled"
		case lastUpdated = "lastUpdated"
		case manner = "manner"
		case rating = "rating"
	}

    init(){
        
    }
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		cleanliness = try values.decodeIfPresent(Int.self, forKey: .cleanliness)
		comment = try values.decodeIfPresent(String.self, forKey: .comment)
		dateCreated = try values.decodeIfPresent(String.self, forKey: .dateCreated)
		driver = try values.decodeIfPresent(Driver.self, forKey: .driver)
		drivingStandard = try values.decodeIfPresent(Int.self, forKey: .drivingStandard)
		enabled = try values.decodeIfPresent(Bool.self, forKey: .enabled)
		lastUpdated = try values.decodeIfPresent(String.self, forKey: .lastUpdated)
		manner = try values.decodeIfPresent(Int.self, forKey: .manner)
		rating = try values.decodeIfPresent(Int.self, forKey: .rating)
	}

}
