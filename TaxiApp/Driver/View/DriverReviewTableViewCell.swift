import UIKit
import Cosmos

class DriverReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var cleanlinesstext: UILabel!
    @IBOutlet weak var mannertext: UILabel!
    @IBOutlet weak var driverstandardtext: UILabel!
    @IBOutlet weak var clean: UILabel!
    @IBOutlet weak var manner: UILabel!
    @IBOutlet weak var stand: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var rateview: CosmosView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cleanlinesstext.text = Language.getString("Cleanliness")
        self.mannertext.text = Language.getString("Manner")
        self.driverstandardtext.text = Language.getString("Driving Standard")
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
