import UIKit
import Alamofire

class ApiDriverLocation {
    func doWork(driverId:String,taxiStandId:String)  {
        let param = [
            "driverId":driverId,
            "taxiStandId":taxiStandId,
            "key":"df2e54de-74c6-44a4-8941-785cb68d6d72"
        ]
        let headers = ["Content-Type": "application/json"]
        Alamofire.request(getUrl(), method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers).responseString { data in
            print(data.value ?? "not data found")
        }
    }
    func getUrl() -> String {
       return "\(Url_.baseurl)api/driverLocation"
    }
}
