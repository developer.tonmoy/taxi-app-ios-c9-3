import UIKit
import Alamofire

class ApiDriverStatus {

    func doWork(isOnline:String,driverid:String){
    let url = "\(Url_.baseurl)api/driverStatus"
    
    let headers = ["Content-Type": "application/json"]
           let param:Parameters = [
               "online": "\(isOnline)",
               "driverId": "\(driverid)",
               "key": "df2e54de-74c6-44a4-8941-785cb68d6d72"
           ]
    
    Alamofire.request(url,method: .post,parameters: param, encoding: JSONEncoding.default, headers: headers).validate()
        .responseString { res in
            res.result.ifSuccess {
                if let str = res.data {
                   let s =  String(data: str, encoding: .utf8)
                    print(s ?? "not found")
                }
            }
    }
    }

}
