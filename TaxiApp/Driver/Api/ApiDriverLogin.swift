import UIKit
import Alamofire

class ApiDriverLogin {

    func doWork(username:String,password:String,loginSuccess:@escaping((Driver)->()) , loginFailed:@escaping(()->()) )  {
       
        let url = "\(Url_.baseurl)api/driverLogin"
        let headers = ["Content-Type": "application/json"]
        let param:Parameters = [
            "username": "\(username)",
            "password": "\(password)"
        ]
        Alamofire.request(url,method: .post,parameters: param,encoding: JSONEncoding.default,
                          headers: headers).validate().responseJSON { res in
                            res.result.ifSuccess {
                                
                                if let data = res.data{
                                    
                                    do{
                                        let data  =  try JSONDecoder().decode(Driver.self, from: data)
                                        loginSuccess(data)
                                    }catch(let error){
                                        print(error.localizedDescription)
                                        loginFailed()
                                    }
                                    
                                }
                                
                            }
                            res.result.ifFailure {
                                if res.response?.statusCode == 400{
                                    loginFailed()
                                }
                            }
        }
    }
  
}
