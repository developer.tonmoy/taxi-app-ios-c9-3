import UIKit
import Alamofire

class ApiDriverReviewList {

    func doWork(driverID:String,onComplete:@escaping([DriverRate])->()) {
       
        let url = "\(Url_.baseurl)api/getDriverRating/\(driverID)"
        Alamofire.request(url, method: .get)
            .validate().responseJSON { res in
            
                res.result.ifSuccess {
                    
                    if let data = res.data{
                        do{
                        let rateList = try JSONDecoder().decode([DriverRate].self, from: data)
                        onComplete(rateList)
                        }catch(let error){
                            print(error.localizedDescription)
                        }
                    }
                    
                }
                res.result.ifFailure {
                    print("ifFailure")
                }
        }
        
    }
    
}
