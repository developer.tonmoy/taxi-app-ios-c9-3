import UIKit
import Cosmos

class DriverReviewViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableview: UITableView!
    var rateList:[DriverRate] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    
        tableview.backgroundColor = .gray
        
        ApiDriverReviewList().doWork(driverID: LocalSaved().getDriverID()) { list in
            self.rateList = list
            self.tableview.reloadData()
        }
        
        
    }
    func configureUI() {
           view.backgroundColor  = .gray

              navigationController?.navigationBar.barTintColor = .gray
              if #available(iOS 11.0, *) {
                  navigationController?.navigationBar.prefersLargeTitles = false
              } else {
                  // Fallback on earlier versions
              }
              navigationItem.title = "Review"
              let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
              navigationController?.navigationBar.titleTextAttributes = textAttributes
             // navigationController?.navigationBar.barStyle = .black
              
              navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "baseline_clear_white_36pt_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleDismiss))
           
           
          }
    @objc func handleDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return rateList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "driver_review_cell") as! DriverReviewTableViewCell
        cell.selectionStyle = .none
        let rate = rateList[indexPath.row]
        cell.backgroundColor = .darkGray
        cell.clean.text = "\(rate.cleanliness ?? 0)/5"
        cell.manner.text = "\(rate.manner ?? 0)/5"
        cell.stand.text = "\(rate.drivingStandard ?? 0)/5"
        cell.comment.text = rate.comment ?? ""
        cell.rateview.rating = Double(rate.rating ?? 0)
        cell.rateview.isUserInteractionEnabled = false
        
        return cell
        
    }
    


}
