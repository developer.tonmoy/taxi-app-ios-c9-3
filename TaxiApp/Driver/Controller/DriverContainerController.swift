import UIKit

class DriverContainerController: UIViewController {
    
    // MARK: - Properties
    
    var menuController: DriverMenuController!
    var centerController: UIViewController!
    var isExpanded = false
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHomeController()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool {
        return isExpanded
    }
    
    // MARK: - Handlers
    
    func configureHomeController() {
        let homeController = DriverHomeController()
        homeController.delegate = self
        centerController = UINavigationController(rootViewController: homeController)
        
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }
    
    func configureMenuController() {
        if menuController == nil {
            menuController = DriverMenuController()
            menuController.delegate = self 
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
        }
    }
    
    func animatePanel(shouldExpand: Bool, menuOption: DriverMenuOption?) {
        
        if shouldExpand {
            // show menu
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
            }, completion: nil)
            
        } else {
            // hide menu
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = 0
            }) { (_) in
                guard let menuOption = menuOption else { return }
                self.didSelectMenuOption(menuOption: menuOption)
            }
        }
        
        animateStatusBar()
    }
    
    func didSelectMenuOption(menuOption: DriverMenuOption) {
        switch menuOption {
        case .Review: do{
            
            let story = UIStoryboard(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "driverreview") as! DriverReviewViewController
                present(UINavigationController(rootViewController: vc),animated: true)
            
            
            }
            
        case .Impressum:do {
            let controller = ImpressumViewController()
           
            present(UINavigationController(rootViewController: controller), animated: true, completion: nil)
        }
           
        case .Logout:do{
            
            LocalSaved().setDriverID("-1")
            let vc = ContainerController()
            self.navigationController?.pushViewController(vc, animated: true)
                
            }
        case .Language_:
            if Language.getCurrentLanguage() == "en"{
                           Language.setCurrentLanguage("de")
                           
                           
                       }else if Language.getCurrentLanguage() == "de"{
                            Language.setCurrentLanguage("en")
                       }
                       if menuController.reload != nil{
                           menuController.reload!()
                       }
           break
        }
    }
    
    func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }
}

extension DriverContainerController: DriverHomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: DriverMenuOption?) {
        if !isExpanded {
            configureMenuController()
        }
        
        isExpanded = !isExpanded
        animatePanel(shouldExpand: isExpanded, menuOption: menuOption)
    }
}
