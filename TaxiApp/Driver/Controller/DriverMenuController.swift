import UIKit

private let reuseIdentifer = "MenuOptionCell"

class DriverMenuController: UIViewController {
    
    // MARK: - Properties
    var reload:(()->())? = nil
    var tableView: UITableView!
    var delegate: DriverHomeControllerDelegate?
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        reload = {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Handlers
    
    func configureTableView() {
        let imageHolderView = UIView()
         imageHolderView.translatesAutoresizingMaskIntoConstraints = false
        imageHolderView.backgroundColor = .systemYellow
        view.addSubview(imageHolderView)
        imageHolderView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imageHolderView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        imageHolderView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        let height_ =  (view.frame.size.height/3)
        imageHolderView.heightAnchor.constraint(equalToConstant:height_).isActive = true
        
        let imageView = UIImageView(image: UIImage(named: "profile"))
        imageView.contentMode = .scaleAspectFill
        let hw:CGFloat = height_/2.5
       
        
        imageView.layer.borderWidth = 1
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.cornerRadius = hw/2
        imageView.clipsToBounds = true
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        imageHolderView.addSubview(imageView)
        
        
        imageView.heightAnchor.constraint(equalToConstant: hw).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: hw).isActive = true
        imageView.centerYAnchor.constraint(equalTo: imageHolderView.centerYAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: imageHolderView.centerXAnchor,constant: -25).isActive = true
        
        
        
        let drivernameText = UITextView()
        
        drivernameText.text = "\(LocalSaved().getFirstName()) \(LocalSaved().getlastName())"
        drivernameText.font = UIFont(name: drivernameText.font!.fontName, size: 18)
        drivernameText.textColor = .black
        drivernameText.backgroundColor = .none
        drivernameText.textAlignment = .center
        
        imageHolderView.addSubview(drivernameText)
        drivernameText.translatesAutoresizingMaskIntoConstraints = false
        
        drivernameText.widthAnchor.constraint(equalTo: imageHolderView.widthAnchor).isActive = true
        drivernameText.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        drivernameText.topAnchor.constraint(equalTo: imageView.bottomAnchor,constant: 10).isActive = true
    
        drivernameText.centerXAnchor.constraint(equalTo: imageHolderView.centerXAnchor,constant: -25).isActive = true
        tableView = UITableView()

        tableView.delegate = self
        tableView.dataSource = self

        tableView.register(MenuOptionCell.self, forCellReuseIdentifier: reuseIdentifer)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.rowHeight = 80

        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: imageHolderView.bottomAnchor).isActive = true
       
        
        let url = "\(Url_.imageurl)\(LocalSaved().getDriverimage())"
        CommonUtils.loadImage(url: url) { img in
            imageView.image = img
        }
        
    }
}

extension DriverMenuController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifer, for: indexPath) as! MenuOptionCell
        
        let menuOption = DriverMenuOption(rawValue: indexPath.row)
        cell.descriptionLabel.text = menuOption?.description
        cell.iconImageView.image = menuOption?.image
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuOption = DriverMenuOption(rawValue: indexPath.row)
        delegate?.handleMenuToggle(forMenuOption: menuOption)
    }
    
}
