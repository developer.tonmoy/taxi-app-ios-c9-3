import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import UserNotifications
import FirebaseDatabase
import GeoFire

class DriverHomeController: UIViewController,CLLocationManagerDelegate ,GMSMapViewDelegate{
    var locationManager:CLLocationManager? = nil
     var placesClient:GMSPlacesClient? = nil
     var mapView:GMSMapView? = nil
     var polyline = GMSPolyline()
     var taxiStandGeoFance:TaxiStandGeoFence? = nil
     var markers:[GMSMarker] = []
     var count:Int = 0
    
     var loginClick:(()->())? = nil
     var orginUpdate:CLLocationCoordinate2D? = nil
     var driverList:[DriverFirebase] = []
     var driverToDriverGeoFire:DriverToDriverGeoFire? = nil
    var drivername:String? = nil
    // MARK: - Properties
    
    var delegate: DriverHomeControllerDelegate?
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       locationManager = CLLocationManager()
       locationManager?.desiredAccuracy = kCLLocationAccuracyBest
       locationManager?.requestAlwaysAuthorization()
       locationManager?.distanceFilter =  10
       locationManager?.startUpdatingLocation()
        
        
        if let initLocation = locationManager?.location{
            taxiStandGeoFance?.findPlace(currentLocation: initLocation)
        }
        
        
          
          locationManager?.delegate = self
          placesClient = GMSPlacesClient.shared()
          markTaxiStand()
          configureNavigationBar()
          
          loginClick = {
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let driverlistVC = storyboard.instantiateViewController(withIdentifier: "driverloginco") as! DriverLoginViewController
               self.navigationController?.pushViewController(driverlistVC, animated: true)
                         
          }
        addOnlineOfflineControl()
        self.drivername = LocalSaved().getDriverName()
        if self.drivername != nil {
            self.driverToDriverGeoFire =  DriverToDriverGeoFire(drivername: self.drivername ?? "No Driver")
            self.driverToDriverGeoFire?.start()
        }
       
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.driverToDriverGeoFire?.stop()
        print("viewWillDisappear")
    }

    
    func makeButton() {
      let btnLaunchAc = UIButton(frame: CGRect(x: 5, y: 150, width: 300, height: 35))
      btnLaunchAc.backgroundColor = .blue
      btnLaunchAc.setTitle("Launch autocomplete", for: .normal)
      btnLaunchAc.addTarget(self, action: #selector(autocompleteClicked), for: .touchUpInside)
      self.view.addSubview(btnLaunchAc)
    }
    @objc func autocompleteClicked(_ sender: UIButton) {
        print(count)
       // let current = CLLocation(latitude: marker[count].position.latitude, longitude: marker[count].position.longitude)
        //taxiStandGeoFance?.findPlace(currentLocation: current)
        
        count += 1
    }
    
    
    
    func addOnlineOfflineControl()  {
        
        let item = [Language.getString("Online"),Language.getString("Offline")]
        let segmentControl =  UISegmentedControl(items: item )
        segmentControl.frame = CGRect(x: 5, y: 100, width: 150, height: 35)
        segmentControl.selectedSegmentIndex = 0
        if #available(iOS 13.0, *) {
            segmentControl.selectedSegmentTintColor = .systemGreen
        }
        driverStarus("1")
        segmentControl.addTarget(self, action: #selector(segmentedValueChanged), for: .valueChanged)
        segmentControl.backgroundColor = .white
             //btnLaunchAc.setTitle("Launch autocomplete", for: .normal)
             //btnLaunchAc.addTarget(self, action: #selector(autocompleteClicked), for: .touchUpInside)
             self.view.addSubview(segmentControl)
       
        
    }
    @objc func segmentedValueChanged(_ sender:UISegmentedControl!)
    {
        if sender.selectedSegmentIndex == 0 {
            driverStarus("1")
            driverToDriverGeoFire?.start()
            markTaxiStand()
            if #available(iOS 13.0, *) {
                sender.selectedSegmentTintColor = .systemGreen
            }
            if let initLocation = locationManager?.location{
            taxiStandGeoFance?.findPlace(currentLocation: initLocation)
            }
            
        }else{
            driverStarus("0")
            if #available(iOS 13.0, *) {
                sender.selectedSegmentTintColor = .systemGray
            }
            driverToDriverGeoFire?.stop()
            
        }
    }
    func driverStarus(_ isonline:String) {
        ApiDriverStatus().doWork(isOnline: isonline, driverid: LocalSaved().getDriverID())
    }
    
    func markTaxiStand(){
           ApiTaxiStandAll().getAll { stands in
            self.markers = []
                      for st in stands{
                          if st.lat != nil && st.lng != nil{
                          let lat = CLLocationDegrees(st.lat!)
                          let lng = CLLocationDegrees(st.lng!)
                          let marker = GMSMarker()
                        
                          let latlon = CLLocationCoordinate2D(latitude:lat ?? 0.0, longitude: lng ?? 0.0)
                          marker.position = latlon
                          marker.title = st.district
                          marker.snippet = st.address
                          marker.userData = st
                          marker.icon = UIImage(named:"standmarker")
                          marker.map = self.mapView
                            
                            self.markers.append(marker)
                          
                           
                          }
                      }
                    self.taxiStandGeoFance = TaxiStandGeoFence(list: self.markers, radius: 40)
            
            
                    if let initLocation = self.locationManager?.location{
                        self.taxiStandGeoFance?.findPlace(currentLocation: initLocation)
                    }
            
            
            
                  }
           
       }
    
    override func loadView() {
              let gframe = CGRect(x: 0, y: 0, width: 1, height: 1)
              let camera = GMSCameraPosition.camera(withLatitude: 48.208133, longitude:16.373937, zoom: 6.0)
               mapView = GMSMapView.map(withFrame: gframe, camera: camera)
               mapView?.delegate = self
               mapView?.isMyLocationEnabled = true
               mapView?.settings.myLocationButton = true
               self.view = mapView
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
           let location: CLLocation = locations.last!
            self.orginUpdate = location.coordinate
            googleMapUpdate(location: location)
            
           
           
       }
    
    func googleMapUpdate(location: CLLocation) {
       /// let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                   //                  longitude: location.coordinate.longitude,zoom: 16)
       let camera =   GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 16, bearing: location.course, viewingAngle: 0.0)
        
       mapView?.animate(to: camera)
        
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
                    UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
                    placesClient?.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: fields, callback: { placeLikelihoodList, error in
                        
                        if let error = error {
                          print("An error occurred: \(error.localizedDescription)")
                          return
                        }

                        if placeLikelihoodList != nil {
                            self.navigationItem.title = placeLikelihoodList?.last?.place.name
                        }
                        
                    })
       
        if taxiStandGeoFance != nil{
            taxiStandGeoFance?.findPlace(currentLocation: location)
        }
        self.driverToDriverGeoFire!
            .updateLocationData(currentlocation: location)
        
        
        
        
        
               
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let ts = marker.userData as! TaxiStand
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let driverlistVC = storyboard.instantiateViewController(withIdentifier: "driverlist") as! DriverListViewController
        driverlistVC.driverid = String(ts.id ?? 0)
        driverlistVC.taxiStand = ts
        driverlistVC.isdriver = true
        self.navigationController?.pushViewController(driverlistVC, animated: true)
        
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.polyline.map = nil
        
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
        
        if orginUpdate == nil{
        placesClient?.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: fields, callback: { placeLikelihoodList , error  in
             print("from location placesClient --------")
            if let error = error {
              print("An error occurred: \(error.localizedDescription)")
              return
            }

            if let placeLikelihoodList_ = placeLikelihoodList {
                if let orgin = placeLikelihoodList_.last?.place.coordinate{
    
                    self.drawPolyline(orgin: orgin, destination:  marker.position)
               
                }
            }
        })
        }else{
            print("from location update --------")
            drawPolyline(orgin: orginUpdate!, destination: marker.position)
        }
        return false
    }
    func drawPolyline(orgin:CLLocationCoordinate2D, destination:CLLocationCoordinate2D){
        ApiPolyline().doWork(orgin:orgin, destination:destination ,onComplete: { points , route in
                           
                           let rect = GMSPath(fromEncodedPath:points )
                           self.polyline.path = rect
                           self.polyline.strokeColor = .blue
                           self.polyline.strokeWidth = 5
                           self.polyline.map = self.mapView
                           
                       })
    }
    
    
    
    
    // MARK: - Handlers
    
    @objc func handleMenuToggle() {
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
    
    func configureNavigationBar() {
         navigationController?.navigationBar.barTintColor = .systemYellow
         navigationController?.navigationBar.barStyle = .black
         navigationController?.navigationBar.barStyle = UIBarStyle.black
         navigationController?.navigationBar.tintColor = UIColor.black
              
         navigationItem.title = "Taxi App Driver"
         let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
              
         navigationController?.navigationBar.titleTextAttributes = textAttributes
         navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_white_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenuToggle))
    }
}
