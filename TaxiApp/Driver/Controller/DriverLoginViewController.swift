import UIKit

class DriverLoginViewController: UIViewController {

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var statusText: UILabel!
    @IBOutlet weak var loginbutton: UIButton!
    @IBOutlet weak var progress: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "login_bg")!)
        setLanguage()
    }
    func setLanguage()  {
        self.username.placeholder = Language.getString("Username")
        self.password.placeholder = Language.getString("Password")
        self.loginbutton.setTitle(Language.getString("Login"), for: .normal) 
    }
    @IBAction func LoginButtonClick(_ sender: UIButton) {
        self.statusText.textColor = .red
        self.statusText.isHidden = false
       
        
        if username.text?.isEmpty ?? true {
            self.statusText.text = Language.getString("ueserempty")
            return
        }
        
        if password.text?.isEmpty ?? true {
            self.statusText.text = Language.getString("Passwordempty")
            
            return
        }
        self.statusText.textColor = .systemYellow
        self.statusText.isHidden = false
        self.statusText.text = Language.getString("Checking")
        self.username.isEnabled = false
        self.password.isEnabled = false
        self.loginbutton.isEnabled = false
        self.progress.startAnimating()
        
        ApiDriverLogin()
            .doWork(username: username.text ?? "" , password: password.text ?? "" , loginSuccess: { driver in
                if self.progress.isAnimating{
                    self.progress.stopAnimating()
                }
                self.statusText.textColor = .green
                self.statusText.text = Language.getString("Loginsuccessful")
                let save =  LocalSaved()
                save.setDriverID(String(driver.id ?? -1))
                save.setDriverName(driver.username ?? "")
                save.setFirstName(driver.firstName ?? "")
                save.setlastName(driver.lastName ?? "" )
                save.setDriverimage(driver.profileImageUrl ?? "" )
                
                self.goToDriverPage()
            
            }) {
               self.statusText.isHidden = false
               self.statusText.textColor = .red
                self.statusText.text = Language.getString("Usernamepasswordincorrect")
               self.username.isEnabled = true
               self.password.isEnabled = true
               self.loginbutton.isEnabled = true
                
                if self.progress.isAnimating {
                    self.progress.stopAnimating()
                }
        }
        
        
    }
    func goToDriverPage()  {
        let vc = DriverContainerController()
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func usernameEdit(_ sender: UITextField) {
         //self.statusText.isHidden = true
    }
    @IBAction func passwordEdit(_ sender: UITextField) {
        // self.statusText.isHidden = true
    }
    

}
